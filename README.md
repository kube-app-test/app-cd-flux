# app-cd-flux

GitOps project on FLUX that should keep a stack of services on kubernetes cluster in sync with the git counterpart.

**Install Kustomize**
```sh
export kustomize_version=kustomize_v4.1.3_linux_arm64.tar.gz
curl -o /tmp/$kustomize_version https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize%2Fv4.1.3/$kustomize_version

sudo tar -xzf /tmp/$kustomize_version -C /usr/local/bin/
source ~/.profile
```
**Install Flux2**
```sh
# https://fluxcd.io/docs/installation/
# https://fluxcd.io/docs/migration/helm-operator-migration/

# Install flux binary
curl -s https://fluxcd.io/install.sh | sudo bash
# Note: flux 0.15.0 seems to have issues with the source-controller. So I'll use 0.14.2

# For 0.14.2
curl -o /tmp/flux.tar.gz -sfL https://github.com/fluxcd/flux2/releases/download/v0.14.2/flux_0.14.2_linux_arm64.tar.gz
tar -xzof /tmp/flux.tar.gz -C /tmp
chmod 755 /tmp/flux
mv /tmp/flux /usr/local/bin/flux
# Add paths, etc.

# Run prechecks
flux check --pre

# Create account token on repository level.
export GITLAB_TOKEN=zzzzzzzzzzzzzzzzzzz

# Bootstrap examples
flux bootstrap gitlab --repository app-cd-flux --owner kube-app-test --path=clusters/staging --personal #--token-auth

# If using ssh key...
flux bootstrap gitlab --ssh-hostname=gitlab.com --repository app-cd-flux --branch test --owner kube-app-test --path=clusters/prod
```

**Automated Image Update**
```sh
Note: Flux only works with immutable image tags (:latest is not supported).
Every image tag must be unique, using commit SHA or semver (ie: 1.2.3)
```


**Overall directory structure**
```sh
.
├── apps			(Helm charts kustomizations based on environment - called from ./clusters/{prod,staging}
│   ├── base
│   │   └── kube-app
│   ├── prod
│   └── staging
├── clusters
│   ├── container-registry	(FLUX image auto update definitions)
│   │   ├── fruits
│   │   ├── products
│   │   ├── redis-api
│   │   └── vegetables
│   ├── prod			(FLUX bootstrap directory + kustomizations of ./apps)
│   └── staging			(FLUX bootstrap driectory + kustomizations of ./apps)
├── infrastructure        	(FLUX Repositories definitions and infra only deployments)
│   ├── ingress
│   ├── rabbitmq
│   ├── redis
│   └── sources
└── scripts			(Not used yet: scripts used for CD)
```
1. **To remove flux from the cluster**
```sh
# Removes flux-system ns and all crd - no impact on running services handled by flux
flux uninstall

Handful one liners:
kubectl patch gitrepo flux-system -n flux-system -p '{"metadata":{"finalizers":[]}}' --type=merge
kubectl delete crd $(kubectl get crd | egrep 'fluxcd.io' | cut -f1 -d' ')
```
##### Getting started
**https://fluxcd.io/docs/get-started/**

##### Kustomize/helm example
**https://github.com/fluxcd/flux2-kustomize-helm-example**